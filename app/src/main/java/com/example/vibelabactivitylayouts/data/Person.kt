package com.example.vibelabactivitylayouts.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Person (
    val secondName: String,
    val firstName: String,
    val patronymic: String,
    val age: Int,
    val hobby: String
) : Parcelable