package com.example.vibelabactivitylayouts.presentation

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.vibelabactivitylayouts.R
import com.example.vibelabactivitylayouts.data.Person
import com.example.vibelabactivitylayouts.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.title = "MainActivity"
        binding.enter.setOnClickListener { onEnterClick() }
    }

    private fun onEnterClick() {
        if (binding.secondName.text.isNotEmpty() ||
            binding.firstName.text.isNotEmpty() ||
            binding.patronymic.text.isNotEmpty() ||
            binding.age.text.isNotEmpty() ||
            binding.hobby.text.isNotEmpty()
        ) {
            val person = Person(
                binding.secondName.text.toString(),
                binding.firstName.text.toString(),
                binding.patronymic.text.toString(),
                binding.age.text.toString().toInt(),
                binding.hobby.text.toString(),
            )
            Intent(this, InfoActivity::class.java).putExtra("person", person)
                .also { startActivity(it) }
        } else Toast.makeText(
            this,
            getString(R.string.full_data),
            Toast.LENGTH_LONG
        ).show()
    }
}