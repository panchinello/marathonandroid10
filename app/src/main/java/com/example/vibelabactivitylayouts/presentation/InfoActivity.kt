package com.example.vibelabactivitylayouts.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.vibelabactivitylayouts.R
import com.example.vibelabactivitylayouts.data.Person
import com.example.vibelabactivitylayouts.databinding.ActivityInfoBinding

class InfoActivity : AppCompatActivity() {

    lateinit var binding: ActivityInfoBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityInfoBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.title = "InfoActivity"

        val person = intent.getParcelableExtra<Person>("person")
        lateinit var ageQualifier: String

        when (person?.age) {
            in 0..17 -> ageQualifier = getString(R.string.age_child)
            in 18..30 -> ageQualifier = getString(R.string.age_young)
            in 31..45 -> ageQualifier = getString(R.string.age_middle)
            in 46..65 -> ageQualifier = getString(R.string.age_elderly)
            in 66..85 -> ageQualifier = getString(R.string.age_senile)
            in 86..Int.MAX_VALUE -> ageQualifier = getString(R.string.age_long_liver)
        }
        binding.infoText.text = getString(
            R.string.full_string,
            ageQualifier,
            person?.age,
            person?.secondName,
            person?.firstName,
            person?.patronymic,
            person?.hobby,
        )
    }
}